from polls.models import Poll, Choice
from django.contrib import admin
# import datetime


class ChoiceInline(admin.StackedInline):
    model = Choice
    extra = 0


class PollAdmin(admin.ModelAdmin):
    fieldsets = [
        (None, {
            'fields': ['question']
        }),
        ('Date information', {
            'fields': ['pub_date'],
            'classes': ['collapse']
        }),
    ]

    inlines = [ChoiceInline]

    list_display = ('question', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']

admin.site.register(Poll, PollAdmin)
